// Soal No 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992]
var objekDaftarPeserta = {
    nama: arrayDaftarPeserta[0],
    "jenis kelamin": arrayDaftarPeserta[1],
    hobi: arrayDaftarPeserta[2],
    "tahun lahir": arrayDaftarPeserta[3],
}
console.log(objekDaftarPeserta);


// Soal No 2
var objekBuah = {
    buah1: {
        nama: "strawberry",
        warna: "merah",
        "ada bijinya": "tidak",
        harga: 9000
    },
    buah2: {
        nama: "jeruk",
        warna: "oranye",
        "ada bijinya": "ada",
        harga: 8000
    },
    buah3: {
        nama: "Semangka",
        warna: "Hijau & Merah",
        "ada bijinya": "ada",
        harga: 10000
    },
    buah4: {
        nama: "Pisang",
        warna: "Kuning",
        "ada bijinya": "tidak",
        harga: 5000
    }
}
console.log(objekBuah.buah1);

// Tugas No 3
var dataFilm = []
function tambahFilm(nama, durasi , genre, tahun){
    var film = {
        nama : nama,
        durasi : durasi,
        genre : genre,
        tahun : tahun
    }
    dataFilm.push(film);
}
tambahFilm("saw 1", "2 jam", "thriller", "2000");
tambahFilm("saw 2", "2 jam", "thriller", "2001");
console.log(dataFilm);


