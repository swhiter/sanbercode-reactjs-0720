//Soal no 1
var hitung = (jariJari) => {
    let luas,keliling;
    const jari2= jariJari;
    luas = 3.14 * jari2 * jari2;
    keliling = 3.14 * 2 * jari2;
    console.log("luas: " + luas + "\nkeliling: " + keliling);
    return () => {
    }
}
hitung(5);



// Soal no 2
let kalimat = ""
const kata1 = "saya";
const kata2 = "adalah";
const kata3 = "seorang";
const kata4 = "frontend";
const kata5 = "developer";
function gabung(kata1,kata2,kata3,kata4,kata5){
    kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`;
    return kalimat;
}
console.log(gabung(kata1,kata2,kata3,kata4,kata5));



// Soal no 3
class Book {
    constructor(name, totalPage, price){
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
}
class Komik extends Book {
    constructor(name, totalPage, price){
        super(name ,totalPage, price)
        this.isColorful = true
    }
    show(){
        return this.name + " " + this.totalPage + " " + this.price
        //return `${this.name} ${this.totalPage} ${this.price}`;
    }
}
buku = new Komik("isi", 100, 20000)
console.log(buku.show())