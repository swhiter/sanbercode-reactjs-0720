// Tugas No. 1
console.log("Looping Pertama");
var i = 2;
while(i <= 20) { 
    console.log(i+ " - I love coding");
    i+=2;
}
console.log("Looping Kedua");
var i = 20;
while(i >= 2) { 
    console.log(i+ " - I will become a fullstack developer");
    i-=2;
}


// Tugas No. 2 
for(var i = 1; i <= 20; i++) {
    if(i%3==0 && i%2!=0){
        console.log(i+ " - I love coding");
    }else if(i%2==0){
        console.log(i+ " - Berkualitas");
    }else{
        console.log(i+ " - Santai");
    }
} 


// Tugas No. 3 
var pagar = "#";
for(var i = 1; i <= 7; i++) {
    console.log(pagar);
    pagar += "#";
}


// Tugas No. 4
var kalimat="saya sangat senang belajar javascript"
var kata = kalimat.split(" ")
console.log(kata)


// Tugas No. 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
console.log(daftarBuah);